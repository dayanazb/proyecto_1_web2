var servicio_id = "ISb9a1cfae5bd347989951071f4a909c1c";

$(document).ready(function() {
    validar_autenticacion();
    obtenerCanales();
});

//Valida si no hay un usuario autenticado y lo envia al login. 
function validar_autenticacion() {
    if (localStorage.getItem('usuario_activo') === null) {
        window.location.href = "index.html";
    }
}


//Listado de los canales y opcion de unirse al canal
function obtenerUbicacion(canales) {
    $('#lista_canales').empty();
    var url = window.location.toString();
    if (~url.indexOf("principal.html")) {
        var list = "<table class='table table-striped'><tr><th>Canal</th><th>Opciones</th></tr>";
        $(canales).each(function(index, value) {
            list += "<tr><td><a href='#' id='" + value.sid + "'>" + value.friendly_name + "</a></td>" +
                "<td><button class='btn btn-danger' id='" + value.sid + "'>Eliminar</button> " +
                "<button data-toggle='modal' data-target='#modalEditChannel' class='btn btn-warning' onclick='editar_canal(" + value.sid + ")'>Editar</button></td></tr>";
        });
        list += "</table>";
    } else if (~url.indexOf("canales.html")) {
        var list = "<ul>";
        $(canales).each(function(index, value) {
            list += "<a href='#' class='list-group-item disabled' >" + value.friendly_name + " <button onclick='unirseAlCanal(`" + value.sid + "`)' style='float: right' type='button' class='btn btn-success'>Unirse</button></a>";
        });
        list += "</ul>";
    } else if (~url.indexOf("canal.html")) {
        obtenerMiembros();
        obtenerMensajes();
    }
    $('#lista_canales').append(list);
    $('.btn-danger').on('click', function(e) {
        var id = $(this).attr('id');
        eliminar_canal(id);
    });
}

//Obtiene los miembros que estan unidos a un canal.
function obtenerMiembros() {
    $.ajax({
            type: "GET",
            dataType: 'json',
            url: "https://chat.twilio.com/v2/Services/" + servicio_id + "/Channels/" + localStorage.getItem('canal_actual') + "/Members",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa("AC106c006d21e1a19fd624d273827ffcc5:4a5ef374dc169361a36f2054b5094a30"));
            }
        })
        .done(function(data) {
            var list = "<ul>";
            $(data.members).each(function(index, value) {
                list += "<a href='#' class='list-group-item disabled' >" + value.identity + "</a>";
            });
            list += "</ul>";
            $('#miembros_div').append(list);
        })
        .fail(function(jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

//Obtiene la fecha para utilizarla en los mensajes
function getDate(date1) {
    var date = new Date(date1);
    var day = (date.getDate() <= 9 ? "0" + date.getDate() : date.getDate());
    var month = (date.getMonth() + 1 <= 9 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1));
    var dateString = date.getHours() + ":" + date.getMinutes() + " " + day + "/" + month + "/" + date.getFullYear();
    return dateString;
}

//Obtiene los mensajes creados en el canal
function obtenerMensajes() {
    $('#chat_div').empty();
    $('#txtMensaje').val('');
    $.ajax({
            type: "GET",
            dataType: 'json',
            url: "https://chat.twilio.com/v2/Services/" + servicio_id + "/Channels/" + localStorage.getItem('canal_actual') + "/Messages",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa("AC106c006d21e1a19fd624d273827ffcc5:4a5ef374dc169361a36f2054b5094a30"));
            }
        })
        .done(function(data) {
            $('txtMensaje').val('');
            var list = "<div class='card' style='width:auto'><div class='card-body'>";
            $(data.messages).each(function(index, value) {
                list += "<h5 class='card-title' style='float:right; color:blue;'><small style='font-size:60%;'>Enviado por: </small>" +
                    value.from + "</h5><small style='color:green'>" + getDate(value.date_created) + "</small><br><p class='card-text'>" + value.body + "</p><hr>";
            });
            list += "</div></div>";
            $('#chat_div').append(list);
        })
        .fail(function(jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

//Salir de la aplicacion
$('#salirBtn').on('click', function(e) {
    localStorage.clear();
    window.location.href = "index.html";
});

//Salir de la aplicacion
$('#atrasBtn').on('click', function(e) {

    window.location.href = "canales.html";
});

//funcion que crea los mensajes en el canal
$('#enviarMensajeBtn').on('click', function(e) {
    var usuario = JSON.parse(localStorage.getItem('usuario_activo'));
    var mensaje = $('#txtMensaje').val();
    if (mensaje !== '') {
        $.ajax({
                type: "POST",
                dataType: 'json',
                url: "https://chat.twilio.com/v2/Services/" + servicio_id + "/Channels/" + localStorage.getItem('canal_actual') + "/Messages",
                data: {
                    "Body": mensaje,
                    "From": usuario.usuario
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa("AC106c006d21e1a19fd624d273827ffcc5:4a5ef374dc169361a36f2054b5094a30"));
                }
            })
            .done(function(data) {
                obtenerMensajes();
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
            });
    } else {
        alert('Debe agregar un mensaje.');
    }
});

//Obtiene los canales de un servicio
function obtenerCanales() {
    $.ajax({
            type: "GET",
            dataType: 'json',
            url: "https://chat.twilio.com/v2/Services/" + servicio_id + "/Channels",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa("AC106c006d21e1a19fd624d273827ffcc5:4a5ef374dc169361a36f2054b5094a30"));
            }
        })
        .done(function(data) {
            obtenerUbicacion(data.channels);
        })
        .fail(function(jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

//Edita los canales 
$('#update_channel_btn').on('click', function(e) {
    var canal_id = $("#canal_id_txt").val();
    var canal_nombre = $("#editar_canal_txt").val().trim();
    if (!canal_nombre.length == 0) {
        $.ajax({
                type: "POST",
                dataType: 'json',
                url: "https://chat.twilio.com/v2/Services/" + servicio_id + "/Channels/" + canal_id,
                data: {
                    "FriendlyName": canal_nombre
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa("AC106c006d21e1a19fd624d273827ffcc5:4a5ef374dc169361a36f2054b5094a30"));
                }
            })
            .done(function(data) {
                $('#close_edit_modal').click();
                obtenerCanales();
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
            });
    }
});

//Crea un canal
function create_channel() {
    var channel = $("#crear_canal_txt").val().trim();
    if (!channel.length == 0) {
        $.ajax({
                type: "POST",
                dataType: 'json',
                url: "https://chat.twilio.com/v2/Services/" + servicio_id + "/Channels",
                data: {
                    "FriendlyName": channel
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa("AC106c006d21e1a19fd624d273827ffcc5:4a5ef374dc169361a36f2054b5094a30"));
                }
            })
            .done(function(data) {
                obtenerCanales();
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
            });
    }
}

//Elimina un canal
function eliminar_canal(canal_id) {
    $.ajax({
            type: "DELETE",
            dataType: 'json',
            url: "https://chat.twilio.com/v2/Services/" + servicio_id + "/Channels/" + canal_id,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa("AC106c006d21e1a19fd624d273827ffcc5:4a5ef374dc169361a36f2054b5094a30"));
            }
        })
        .done(function(data) {
            obtenerCanales();
        })
        .fail(function(jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
}

//Obtiene el id del canal y el texto del input
function editar_canal(canal) {
    $('#canal_id_txt').val(canal.id);
    $('#editar_canal_txt').val(canal.innerText);
}


//Permite que el usuario se una a un canal
function unirseAlCanal(canal_id) {
    console.log(canal_id);
    var usuario = JSON.parse(localStorage.getItem('usuario_activo'));
    $.ajax({
            type: "POST",
            dataType: 'json',
            url: "https://chat.twilio.com/v2/Services/" + servicio_id + "/Channels/" + canal_id + "/Members",
            data: {
                "Identity": usuario["usuario"]
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa("AC106c006d21e1a19fd624d273827ffcc5:4a5ef374dc169361a36f2054b5094a30"));
            }
        })
        .done(function(data) {
            localStorage.setItem('canal_actual', canal_id);
            window.location.href = "canal.html";
        })
        .fail(function(jqxhr, textStatus, error) {
            console.log(error);
            localStorage.setItem('canal_actual', canal_id);
            window.location.href = "canal.html";
        });
}