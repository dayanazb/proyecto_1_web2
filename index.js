$('document').ready(function() {
    //Guarda usuarios al iniciar aplicación
    localStorage.clear();
    if (localStorage.getItem("usuarios") === null) {
        var usuarios = {
            "usuarios": [{
                    "usuario": "dayana",
                    "contrasena": "1234",
                    "tipo": "admin"
                }, {
                    "usuario": "juan",
                    "contrasena": "1234",
                    "tipo": "usuario"
                }, {
                    "usuario": "carlos",
                    "contrasena": "1234",
                    "tipo": "usuario"
                },
                {
                    "usuario": "maria",
                    "contrasena": "1234",
                    "tipo": "usuario"
                },
                {
                    "usuario": "ana",
                    "contrasena": "1234",
                    "tipo": "usuario"
                },
                {
                    "usuario": "jose",
                    "contrasena": "1234",
                    "tipo": "usuario"
                },
                {
                    "usuario": "Isabel",
                    "contrasena": "1234",
                    "tipo": "usuario"
                },
                {
                    "usuario": "xinia",
                    "contrasena": "1234",
                    "tipo": "usuario"
                }
            ]
        }
        localStorage.setItem('usuarios', JSON.stringify(usuarios));
    }
    if (localStorage.getItem('usuario_activo') !== null) {
        window.location.href = "canales.html";
    }

    //Detiene evento de form para comprobar si el usuario existe
    $('#iniciarBtn').on('click', function(e) {
        e.preventDefault();
        var usuario = $('#usuario').val();
        var contrasena = $('#contrasena').val();
        if (usuario != '' || contrasena != '') {
            var usuarios = JSON.parse(localStorage.getItem('usuarios'));
            $(usuarios.usuarios).each(function(index, us) {
                if (usuario == us.usuario && contrasena == us.contrasena) {
                    localStorage.setItem('usuario_activo', JSON.stringify(us));
                    if (us.tipo === "usuario") {
                        window.location.href = "canales.html";
                    } else {
                        window.location.href = "principal.html";
                    }
                }
            });
            setTimeout(() => {
                $('#lblError').attr('style', 'display: block !important');
            }, 1000);

        }
    });

});